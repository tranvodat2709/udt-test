const config = {
    tabWidth: 4,
    semi: false,
    singleQuote: true,
    jsxSingleQuote: false,
    bracketSpacing: true,
    bracketSameLine: true,
    jsxBracketSameLine: true
};