import React from "react"
import ButtonsCalculator from "../component/Button/ButtonsCalculator";
import { CalculatorContext } from "../context/CalculatorProvider";
import { useContext } from "react";
import { Link } from "react-router-dom";

const buttonCal = [
    ["AC", "+/-", "%", "/"],
    [7, 8, 9, "x"],
    [4, 5, 6, "-"],
    [1, 2, 3, "+"],
    [0, ".", "="],
];

function Calculator() {
    const contextValue = useContext(CalculatorContext);


    if (!contextValue) {
        return <div>Calculator context not provided.</div>;
    }

    const { calculator, setCalculator } = contextValue

    const handleKeyDown = (event: any) => {
        const key = event.key;

        if (!(key.match(/[\d\+\-\*\/]/) || key === 'Backspace' || key === 'Enter')) {
            event.preventDefault();
        }
    };

    const handleEdit = (event: any) => {
        const editedValue = event.target.innerText;
        const numericValue = editedValue.replace(/[^0-9]/g, '');

        setCalculator({
            ...calculator,
            number: numericValue === '' ? '0' : numericValue,
        });

        // Explicitly set innerHTML to '0' when numericValue is empty
        if (numericValue === '') {
            event.target.innerHTML = '0';
        }
    };
    return (
        <main className="container">
            <div className='wrapper '>
                <div className="calculator">
                    <div className=" calculator-layout">
                        <div className="screen-calculator">
                            <span className="screen-calculator-light">
                                <div></div>
                                <div></div>
                                <div></div>
                            </span>
                            <div className="screen-calculator-main">
                                <span className="screen-calculator-cal">
                                    {calculator.sign && (calculator.result + "" + calculator.sign)}
                                </span>
                                <div className="screen-calculator-primary">
                                    <div contentEditable={true}
                                        onInput={handleEdit}
                                        onKeyDown={handleKeyDown}
                                        suppressContentEditableWarning={true}
                                        className=""
                                    >
                                        {calculator.number ? calculator.number : calculator.result}
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div className="button-container">
                            {buttonCal.flat().map((value: any) => (
                                <ButtonsCalculator value={value}></ButtonsCalculator>
                            ))
                            }
                        </div>
                    </div>

                </div>

            </div>
        </main>


    );
};

export default Calculator;
