import React from "react"
import { useContext } from "react";
import { CalculatorContext } from "../context/CalculatorProvider";

export default function History() {
    const contextValue = useContext(CalculatorContext);

    if (!contextValue) {
        return <div>Calculator context not provided.</div>;
    }
    const { calculator } = contextValue
    return (
        <h1>
            <h1 className="history-heading">
                History of Calculation
            </h1>
            <div className="history-wrapper">
                <div>
                    {calculator.historyCollection.map((history: string) => (
                        <div className="history-item">
                            {history}
                        </div>
                    ))}
                </div>
            </div>
        </h1>
    )
}