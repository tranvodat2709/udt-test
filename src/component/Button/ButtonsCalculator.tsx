import React from "react"

import { CalculatorContext } from "../../context/CalculatorProvider"
import { useContext } from "react"

const getOperatorsClassName = (btn: '=' | 'x' | '-' | '+' | '/') => {
  const className = {
    '=': 'operator equals',
    'x': 'operator',
    '-': 'operator',
    '+': 'operator',
    '/': 'operator',
    '%': 'percent-sign',
    "+/-": 'change-sign',
    "AC": "all-clear",
    0: "zero",
    1: "natural-number",
    2: "natural-number",
    3: "natural-number",
    4: "natural-number",
    5: "natural-number",
    6: "natural-number",
    7: "natural-number",
    8: "natural-number",
    9: "natural-number",
    '.': "decimal-point"

  }
  return className[btn]
}

const ButtonsCalculator = ({ value }: any) => {
  const contextValue = useContext(CalculatorContext);


  if (!contextValue) {
    return <div>Calculator context not provided.</div>;
  }

  const { calculator, setCalculator } = contextValue

  const calc = (a: number, b: number, sign: string) => {
    if (sign) {
      const result: any = {
        '+': (a: number, b: number) => a + b,
        "-": (a: number, b: number) => Math.round((a - b) * 1e12) / 1e12,
        "x": (a: number, b: number) => Math.round((a * b) * 1e12) / 1e12,
        "/": (a: number, b: number) => a / b,
      }
      return result[sign](a, b);
    } else {
      return (+a) + (+b)
    }
  }

  const handleDecimalPoint = () => {
    setCalculator({
      ...calculator,
      number: !calculator.number.toString().includes('.') ? calculator.number + value : calculator.number
    });
  }


  const handleAllClear = () => {
    setCalculator({
      ...calculator,
      sign: '',
      number: 0,
      result: 0,
      historyCalc: null,
    })
  }

  const handleClickNumber = () => {
    const numberString = value.toString()
    let numberValue;

    if (numberString === '0' && calculator.number === 0) {
      numberValue = 0
    } else {
      numberValue = Number(calculator.number + numberString)
    }

    if (!calculator.sign && calculator.result) {
      setCalculator({
        ...calculator,
        result: 0,
        number: numberValue,
        historyCalc: (calculator.historyCalc === null) ? numberString : (calculator.historyCalc + numberString)
      })
    } else {
      setCalculator({
        ...calculator,
        number: numberValue,
        historyCalc: (calculator.historyCalc === null) ? numberString : (calculator.historyCalc + numberString)
      })
    }
  }

  const handleSignClick = () => {
    setCalculator((prevCalculator: any) => {
      //define resultReturn
      let resultReturn;
      if (!prevCalculator.result && prevCalculator.number) {
        resultReturn = prevCalculator.number

      } else if (prevCalculator.result && prevCalculator.number) {
        resultReturn = calc(prevCalculator.result, prevCalculator.number, prevCalculator.sign)

      } else {
        resultReturn = prevCalculator.result
      }

      //Check sign
      let signReturn;
      const historyString = prevCalculator.historyCalc;
      const lastIndex = historyString && (historyString.length - 1);

      if (historyString === null && !prevCalculator.result) {
        signReturn = `0${value}`
      } else if (historyString === null && prevCalculator.result) {
        signReturn = `${prevCalculator.result}${value}`
      } else if (
        lastIndex >= 0 &&
        (historyString[lastIndex] === "-" ||
          historyString[lastIndex] === "+" ||
          historyString[lastIndex] === "x" ||
          historyString[lastIndex] === "/"
        )
      ) {
        signReturn = historyString.slice(0, lastIndex) + value;
      }
      else {
        signReturn = historyString + value;
      }

      return {
        ...prevCalculator,
        sign: value,
        number: 0,
        result: resultReturn,
        historyCalc: signReturn,
      }
    })
  }

  const handleEqualsClick = () => {
    const newResult = calc(calculator.result, calculator.number, calculator.sign);
    const newHistoryCalc = `${calculator.historyCalc}=${newResult}`;
    const prevHistoryCollection = calculator.historyCollection || []
    const newArray = [...prevHistoryCollection, newHistoryCalc]

    setCalculator({
      result: newResult,
      sign: "",
      number: 0,
      historyCalc: null,
      historyCollection: newArray
    });
  };

  const handlePercentSign = () => {
    setCalculator((prevCalculator: any) => {
      let resultReturn;
      let numberReturn
      if (prevCalculator.result && prevCalculator.number) {
        numberReturn = prevCalculator.number / 100
        resultReturn = prevCalculator.result
      } else {
        numberReturn = prevCalculator.number / 100
        resultReturn = prevCalculator.result / 100
      }
      return {
        ...calculator,
        number: numberReturn,
        result: resultReturn
      }
    })
  }

  const toggleNegAndPos = () => {
    if (calculator.result && calculator.number) {
      setCalculator({
        ...calculator,
        number: (calculator.number * -1)
      })
    } else {
      setCalculator({
        ...calculator,
        number: calculator.number ? (calculator.number * -1) : 0,
        result: calculator.result ? (calculator.result * -1) : 0
      })
    }

  }

  const zeroClick = () => {
    if (calculator.result && !calculator.sign && !calculator.number) {
      setCalculator({ ...calculator, sign: '', number: 0, result: 0 })
    } else {
      handleClickNumber();
    }
  }

  const handleCalculatorClick = () => {
    const results: any = {
      '.': handleDecimalPoint,
      "AC": handleAllClear,
      "/": handleSignClick,
      "x": handleSignClick,
      "+": handleSignClick,
      "-": handleSignClick,
      "=": handleEqualsClick,
      "%": handlePercentSign,
      "+/-": toggleNegAndPos,
      "0": zeroClick,
    }
    if (results[value]) {
      return results[value]()
    }
    else {
      return handleClickNumber()
    }
  }

  return (
    <button
      key={value}
      className={`${getOperatorsClassName(value)} button-calculator`}
      onClick={handleCalculatorClick}
    >
      {value === "." ? ","
        : (value === "AC" && (calculator.number !== 0 || calculator.number !== 0 && calculator.result !== 0)) ? "C"
          : value
      }
    </button>
  )
}

export default ButtonsCalculator;
