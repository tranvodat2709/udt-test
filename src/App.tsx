import React from 'react';
import AppContext from './context/AppContext';
import Home from './pages/Home';
import History from './pages/History';
import { Outlet, Route, Routes } from 'react-router-dom';
import { Link } from 'react-router-dom';
import CalculatorProvider from './context/CalculatorProvider';
import Calculator from './pages/Calculator';

function App() {
    return (
        <AppContext>
            <CalculatorProvider>
                <div className="app">
                    <div className='route-link'>
                        <Link to="/about" className='link'>About</Link>
                        <Link to="/calculator" className='link'>Calculator</Link>
                        <Link to="/history" className='link'>History</Link>

                    </div>



                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route path='/calculator' element={<Calculator />}></Route>
                        <Route path='/history' element={<History />}></Route>
                    </Routes>
                    <Outlet />
                </div>
            </CalculatorProvider>
        </AppContext>
    );
}

export default App;
