import React, { createContext, useState } from "react";

export const GlobalContext = createContext<string | null>(null);

export default function AppContext({ children }: any) {
    const [user, setUser] = useState<string | null>("Hello");

    return (
        <GlobalContext.Provider value={user}>
            {children}
        </GlobalContext.Provider>
    );
}