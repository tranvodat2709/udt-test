import React, { createContext, useState, useEffect, Dispatch, SetStateAction } from 'react';

interface CalculatorProviderProps {
    children: React.ReactNode;
}

interface CalculatorContextProps {
    calculator: {
        sign: string;
        number: number;
        result: number;
        historyCalc: string | null;
        historyCollection: string[];
    };
    setCalculator: Dispatch<SetStateAction<{
        sign: string;
        number: number;
        result: number;
        historyCalc: string | null;
        historyCollection: string[];
    }>>;
}

export const CalculatorContext = createContext<CalculatorContextProps | null>(null);

const CalculatorProvider: React.FC<CalculatorProviderProps> = ({ children }) => {
    const storedHistoryCollection = localStorage.getItem('historyCollection');
    const initialHistoryCollection: string[] = storedHistoryCollection && JSON.parse(storedHistoryCollection)


    const [calculator, setCalculator] = useState({
        sign: '',
        number: 0,
        result: 0,
        historyCalc: null,
        historyCollection: initialHistoryCollection || [],
    });

    useEffect(() => {
        // Update local storage whenever historyCollection changes
        localStorage.setItem('historyCollection', JSON.stringify(calculator.historyCollection));
    }, [calculator.historyCollection]);

    const providerValue: any = { calculator, setCalculator };

    return (
        <CalculatorContext.Provider value={providerValue}>
            {children}
        </CalculatorContext.Provider>
    );
};

export default CalculatorProvider;
