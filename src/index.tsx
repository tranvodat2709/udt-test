import React from 'react';
import "./style.css";
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import ReactDOM from 'react-dom';

const root = document.getElementById('root') as HTMLElement
ReactDOM.hydrate(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    root
);