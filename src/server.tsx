import React from 'react';
import express from 'express';
import { StaticRouter } from 'react-router-dom/server';
import ReactDOMServer from 'react-dom/server';
import App from './App';
import fs from 'fs';
import About from './pages/About';

const app = express();
app.use('/static', express.static(__dirname));
const PORT = process.env.PORT || 3000;

const createReactApp = async (location: any) => {
  let reactApp;

  switch (location) {
    case '/about':
      reactApp = ReactDOMServer.renderToString(
        <StaticRouter location={location}>
          <About />
        </StaticRouter>
      );
      break;
    default:
      reactApp = ReactDOMServer.renderToString(
        <StaticRouter location={location}>
          <App />
        </StaticRouter>
      );
      break;
  }

  const html = await fs.promises.readFile(`${__dirname}/index.html`, 'utf-8');
  const reactHtml = html.replace(
    '<div id="root"></div>',
    `<div id="root">${reactApp}</div>`
  );
  return reactHtml;
};





app.get('*', async (req, res) => {
  const indexHtml = await createReactApp(req.url);
  res.status(200).send(indexHtml);
});

app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}, http://localhost:${3000}/`);
});
